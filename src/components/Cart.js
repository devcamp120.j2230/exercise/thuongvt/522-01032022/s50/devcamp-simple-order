import { Button, Card, CardActions, CardContent, Container, Grid, Typography } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";

const Cart = () => {
    //lấy giá trị từ reducer
    const { mobileList, totalValue } = useSelector((reduxData) => {
        //gọi đến taskReducer
        return reduxData.taskReducer;
    });

    //cho phép lấy giá trị từ view đưa về reducer
    const dispatch = useDispatch();

    const onClickBuy = (index)=>{
        dispatch({
            type: "BUTTON_CLICK",
            payload: index
        })
    };

    return (
        <Container>
            <Grid container>
                {mobileList.map((value, i)=>{
                    return <Grid item xs={12} md={4} sm={12} p={5} key={i}>
                                <Card>
                                    <CardContent>
                                        <Typography variant="h5" component="div">{value.name}</Typography>
                                        <Typography component="div" color="text.secondary">Price: {value.price} USD</Typography>
                                        <Typography component="div" color="text.secondary">Quantity: {value.quantity}</Typography>
                                    </CardContent>
                                    <CardActions>
                                        <Button variant="contained" color="success" onClick={() => onClickBuy(i)}>Buy</Button>
                                    </CardActions>
                                </Card>
                            </Grid>
                })}
            </Grid>
            <Grid container>
                <Grid sx={{ fontWeight: 'bold' }} component="div">Total: {totalValue} USD</Grid>
            </Grid>
        </Container>
    )
}

export default Cart;